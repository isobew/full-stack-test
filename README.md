## Teste Desenvolvedor Full Stack

O teste consiste em criar uma aplicação com Backend(Laravel || NodeJS) que expõe uma API REST de um CRUD de usuários e filmes e uma aplicação web contendo uma interface(React/Next.JS) para login e acesso a dados de uma API externa.

# Back-end
    •  Todos os endpoints de consulta de dados devem ter autenticação por Token ou similar

# Front-end
O front-end deverá ser desenvolvido em React deve apresentar pelo menos os seguintes requisitos:
    •  Interface de login
    •  Feedbacks de usuário ou senha incorreta
    •  Listagem dos dados de filmes
    •  Paginação dos dados
    •  Listagem dos dados de Usuários

# Critérios de avaliação
    •  Funcionamento do projeto
    •  Estrutura do código
    •  Uso de boas práticas
    •  Cumprimento dos requisitos mínimos

# Deve ser entregue:
    •  Um repositório git (fork deste)
    •  Criação de um Readme com instruções de build

Não se deve fazer o commit de pastas como node_modules, o projeto deve instalar suas dependências a partir do package.json

# Extras:
    •  Publicação no Vercel.app
    •  Uso de Containers Docker
    •  Uso de Testes
    •  Build para produção

# Este projeto foi desenvolvido com Laravel 10 e React:
## As ferramentas e bibliotecas utilizadas foram:
    •  Git/GitLab para versionamento;
    •  VSCode para edição e desenvolvimento de código.

    No Back:
    •  Linguagem PHP;
    •  Laravel Sanctum para autenticação.
    
    No Front
    •  Vite;
    •  Linguagem Typescript;
    •  Axios para fazer requisições;
    •  Events: Um pacote para trabalhar com eventos;
    •  Formik para criar formulários em React;
    •  React-router-dom para roteamento;
    •  Styled-components para estilização;
    •  Yup para validação de esquemas.

## Como instalar:

#### Faça o clone do repositório:
    git clone https://gitlab.com/isobew/full-stack-test.git

#### No terminal, entre no diretório do projeto e no diretório do front-end:
    cd full-stack-test
    cd front-end

#### Faça a instalação das dependências do front-end e rode o projeto:
    npm install
    npm run dev

#### Em outro terminal, entre no diretório do projeto e no diretório do back-end
    cd full-stack-test
    cd back-end

#### Faça a instalação das dependências do composer:
    composer install

#### Copie o arquivo .env e gere a chave da aplicação:
    cp .env.example .env
    php artisan key:generate

#### Rode as migrations com as seeds e rode o back-end:
    php artisan migrate --seed
    php artisan serve

## Neste projeto está sendo gerado dados em seeds tanto para a lista de filmes, quanto para o usuário:
## Para fazer login, entre com o usuário e senha abaixo:
    •  Entre em: http://localhost:5173/
    •  e-mail: test@example.com
    •  senha: password

#### A aplicação tem as seguintes páginas:
    •  Login;
    •  Home;
    •  Perfil;
    •  Detalhe de um filme;