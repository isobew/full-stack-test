import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import GlobalStyle from "./styles/globalStyles";

import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Container from "./components/Container";
import Message from "./components/Message";

import Login from "./pages/Auth/Login";
import Home from "./pages/Home";
import NotFound from "./pages/NotFound";
import Profile from "./pages/Profile";
import MovieDetails from "./pages/MovieDetails";

import { UserProvider } from "./context/UserContext";

function App() {
  return (
    <Router>
      <GlobalStyle />
      <UserProvider>
        <Navbar />
        <Message />
        <Container>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/movies/:id" element={<MovieDetails />} />
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Container>
        <Footer />
      </UserProvider>
    </Router>
  );
}

export default App;