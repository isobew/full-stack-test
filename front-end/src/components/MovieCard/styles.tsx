
import styled from "styled-components";

export const MovieCard = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
    background-color: #c4c4c4;
    border-radius: 10px;
    
    h3{
        color: #353535;
        font-size: 1.6em;
        padding: 8px;
    }
`

export const MovieCardImage = styled.div`
    background-size: cover;
    background-position: center;
    height: 200px;
    width: 200px;
    margin-bottom: 1.2em;
    border-radius: 10px 10px 0 0;
`

export const MovieText = styled.p`
    display: flex;
    flex-direction: column;
    color: #353535;
    margin-bottom: 1em;
    font-size: 0.7em;
    padding: 8px;
`