import * as S from "./styles";

interface MovieCardTypes {
  title: string;
  description: string;
  rating: string;
  image: string;
}

function MovieCard({
  title,
  description,
  rating,
  image,
}: MovieCardTypes) {
  return (
    <S.MovieCard>
      <S.MovieCardImage
        style={{ backgroundImage: `url(${image})`}}
      ></S.MovieCardImage>
      <h3>{title}</h3>
      <S.MovieText>
        <span>Descrição: {description}</span> 
        <span>Avaliações: {rating}</span> 
      </S.MovieText>
    </S.MovieCard>
  );
}

export default MovieCard;
