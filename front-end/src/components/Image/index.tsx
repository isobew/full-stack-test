import * as S from "./styles";

interface ImageTypes {
  src: string;
  alt: string;
  width?: string;
  style?: any;
}

function Image({ src, alt, width, style }: ImageTypes) {
  return <S.Image style={style} src={src} alt={alt} width={width} />;
}

export default Image;
