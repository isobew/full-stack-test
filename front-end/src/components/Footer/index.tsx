import * as S from "./styles";

function Footer() {
  return (
    <S.Footer>
      <p>
        <span className="bold">Movies Application</span> &copy; 2024
      </p>
    </S.Footer>
  );
}

export default Footer;
