import styled from "styled-components";

export const Container = styled.main`
    min-height: 78vh;
    padding: 2em 2em;
`