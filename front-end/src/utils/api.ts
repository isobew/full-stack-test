import axios from "axios";

// Configuração do Axios
const api = axios.create({
  baseURL: "http://localhost:8000",
});

export default api;