import styled from "styled-components";

export const LoginContainer = styled.section`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
