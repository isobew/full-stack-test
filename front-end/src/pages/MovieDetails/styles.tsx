import styled from "styled-components";

export const MovieDetailsContainer = styled.section`
    p {
        margin-bottom: 1em;
    }
`

export const MovieDetailsHeader = styled.div`
    padding-top: 500px;
    margin-bottom: 2em;
    display: flex;
    justify-content: space-between;
`

export const MovieImageContainer = styled.div`
    position: absolute;
    top: 60px;
    right: 0;
    z-index: -1;
    width: 100%;
    height: 500px;
    background-color: red;
    margin-bottom: 1em;
`
