import * as S from "./styles";
import { useState, useEffect } from "react";
import { Navigate, useParams } from "react-router-dom";
import api from "../../utils/api";
import Image from "../../components/Image";

function MovieDetails() {
  const [movie, setMovie] = useState<any>({});
  const { id } = useParams();
  const [token] = useState(localStorage.getItem("token") || "");

  useEffect(() => {
    api
      .get(`/api/movies/${id}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(token)}`,
        },
      })
      .then((response) => {
        setMovie(response.data.movie);
      });
  }, [token, id]);

  if (!token) {
    return <Navigate to="/notfound" />;
  }

  return (
    <>
      {movie.title && (
        <S.MovieDetailsContainer>
          <S.MovieImageContainer>
            <Image
              style={{ width: "100%", height: "100%", borderRadius: "0" }}
              src={movie.image}
              alt={movie.title}
            />
          </S.MovieImageContainer>
          <S.MovieDetailsHeader>
            <h1>{movie.title}</h1>
            <p>
                <span>Avaliação: </span>
                {movie.rating}
            </p>
          </S.MovieDetailsHeader>
          <p>
            <span>Descrição: </span>
            {movie.description}
          </p>
        </S.MovieDetailsContainer>
      )}
    </>
  );
}

export default MovieDetails;
