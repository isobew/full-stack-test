import api from "../../utils/api";
import { useState, useEffect } from "react";

import Input from "../../components/Form/Input";
import * as S from "./styles";
import Image from "../../components/Image";
import { Navigate } from "react-router-dom";

function Profile() {
  const [user, setUser] = useState<any>({});
  const [tokenData, setToken] = useState("");
  const token = localStorage.getItem("token");

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      setToken(token);
    }
  }, []);

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const response = await api
          .get("/api/getUserData", {
            headers: {
              Authorization: `Bearer ${JSON.parse(tokenData)}`,
            },
          });
          setUser(response.data);
          console.log(response.data);
      } catch (error) {
        console.error("Erro ao buscar usuário:", error);
      }
    }
    
    fetchUser();
  }, [tokenData]);

  if (!token) {
    return <Navigate to="/notfound" />;
  }

  return (
    <section>
      <S.ProfileHeader>
        <h1>Dados do perfil</h1>
        {user.photo && (
          <Image
            src={user.photo}
            alt={user.name}
          />
        )}
      </S.ProfileHeader>
      <S.FormContainer 
      >
        <Input
          text="E-mail"
          type="email"
          value={user.email}
          disabled={true}
        />
        <Input
          text="Nome"
          type="text"
          value={user.name}
          disabled={true}
        />
      </S.FormContainer>
    </section>
  );
}

export default Profile;