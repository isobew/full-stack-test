function NotFound() {

    return (
      <section>
        <h1>404</h1>
        <p>Página não encontrada :(</p>
      </section>
    )
  }
  
  export default NotFound