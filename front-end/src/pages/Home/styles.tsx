import styled from "styled-components";

export const MovieHomeHeader = styled.div`
  margin-bottom: 2em;

  h1 {
    margin-bottom: 0.3em;
  }
`;

export const MovieContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  justify-content: center;

  a {
    margin-top: 20px;
    text-decoration: none;
    background-color: #fff;
    color: #353535;
    font-family: "poppins", sans-serif;
    font-weight: 400;
    cursor: pointer;
    transition: 0.5s;
    padding: 5px 12px;
    border-radius: 5px;
    border: 2px solid #353535;
    font-size: 1.1em;

    &:hover {
      background-color: #353535;
      color: #fff;
    }
  }
`;

export const MovieCardContainer = styled.div`
  margin: 1.5%;
  display: flex;
  flex-direction: column;
  text-align: center;
`;

export const PaginationButton = styled.button`
  width: 80px;
  height: 40px;
  background-color: pink;
  color: #353535;
  border-radius: 10px;
  border-style: none;
  font-weight: bold;
`

export const MoviePageBlock = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const PaginationBlock = styled.section`
  ul {
    display: flex;
    gap: 20px;

    li {
      list-style: none;
    }
  }
  margin: 50px;
`