import { useContext, useEffect, useState } from "react";
import MovieCard from "../../components/MovieCard";
import * as S from "./styles";
import api from "../../utils/api";
import { Context } from "../../context/UserContext";
import { Link } from "react-router-dom";

interface MoviesTypes {
  title: string;
  id: string;
  description: string;
  rating: string;
  image: string;
}

function Home() {
  const [movies, setMovies] = useState([]);
  const { authenticated } = useContext(Context);
  const [tokenData, setToken] = useState("");
  const [info, setInfo] = useState<any>({});
  const url = "/api/movies";

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      setToken(token);
    }
  }, []);

  const fetchMovies = async (url: string) => {
    try {
      if (authenticated) {
        const response = await api.get(url, {
          headers: {
            Authorization: `Bearer ${JSON.parse(tokenData)}`,
          },
        });
        console.log(response);
        setMovies(response.data.movies.data);
        setInfo(response.data.movies);
      }
    } catch (error) {
      console.error("Erro ao buscar filmes:", error);
    }
  };

  const handleNextPage = () => {
    fetchMovies(info.next_page_url);
  };

  const handlePreviousPage = () => {
    fetchMovies(info.prev_page_url);
  };

  useEffect(() => {
    fetchMovies(url);
  }, [tokenData]);

  return (
    <section>
      <S.MovieHomeHeader>
        <h1>Assista seus filmes</h1>
        {!authenticated ? (
          <p>
            <a href="/login">Faça login</a>
          </p>
        ) : (
          ""
        )}
      </S.MovieHomeHeader>
      {authenticated ? (
        <S.MoviePageBlock>
          <S.MovieContainer>
            {movies.length > 0 &&
              movies.map((movie: MoviesTypes) => (
                <S.MovieCardContainer key={movie.id}>
                  <Link to={`/movies/${movie.id}`}>
                    <MovieCard
                      title={movie.title}
                      description={movie.description}
                      image={movie.image}
                      rating={movie.rating}
                    />
                  </Link>
                </S.MovieCardContainer>
              ))}
            {movies.length === 0 && (
              <p>Não há filmes disponíveis no momento.</p>
            )}
          </S.MovieContainer>

          <S.PaginationBlock>
            <ul>
              {info.prev_page_url ? (
                <li>
                  <S.PaginationButton onClick={handlePreviousPage}>
                    Anterior
                  </S.PaginationButton>
                </li>
              ) : null}
              {info.next_page_url ? (
                <li>
                  <S.PaginationButton onClick={handleNextPage}>
                    Próximo
                  </S.PaginationButton>
                </li>
              ) : null}
            </ul>
          </S.PaginationBlock>
        </S.MoviePageBlock>
      ) : (
        ""
      )}
    </section>
  );
}

export default Home;
