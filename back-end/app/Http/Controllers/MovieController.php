<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MovieController extends Controller
{
    public function getAll()
    {
        $movies = Movie::paginate(10);

        return response()->json(['movies' => $movies], 200);
    }

    public function getMovieById($id)
    {
        //verificando se o id é válido
        if (!ctype_xdigit($id) || !($movie = Movie::find($id))) {
            return response()->json(['message' => 'ID inválido ou filme não encontrado!'], 404);
        }

        return response()->json(['movie' => $movie], 200);
    }

}
