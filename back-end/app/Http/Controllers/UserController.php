<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Log;

class UserController extends Controller
{
    public function login(Request $request)
    {
        if(!Auth::attempt($request->only('email', 'password'))) {
            return response(['message' => 'Usuário ou senha inválido'], status: Response::HTTP_UNAUTHORIZED);
        };
        
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = Auth::user();

        $token = $user->createToken('token')->plainTextToken;

        $cookie = cookie('jwt', $token, 60 * 24);
           
        return response()->json([
            'message' => 'Login realizado com sucesso',
            'token' => $token,
            'userId' => $user->id,
        ])->withCookie($cookie);
    }

    public function getUserData(Request $request)
    {
        $userData = Auth::user();

        if ($userData) {
            $user = User::find($userData->id)->select('id', 'name', 'email', 'photo')->first();
            return response()->json($user);
        } else {
            return response()->json(['error' => 'Usuário não autenticado'], 401);
        }
    }
}
