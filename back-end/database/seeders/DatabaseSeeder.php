<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Movie;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
            'photo' => 'https://github.com/isobew/isobew/assets/78851164/ae37a939-3b70-423d-b2df-0aaecaed449a'
        ]);

        for ($i = 1; $i <= 50; $i++) {
            Movie::create([
                'title' => 'Movie ' . $i,
                'description' => 'Description for Movie ' . $i,
                'rating' => rand(1, 10) / 2,
                'image' => 'https://github.com/isobew/isobew/assets/78851164/68de8804-63cd-4afe-becb-cf893a133448',
            ]);
        }
    }
}
